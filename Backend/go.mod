module main

go 1.13

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jackc/pgx/v4 v4.15.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/segmentio/kafka-go v0.4.30
	golang.org/x/crypto v0.0.0-20220315160706-3147a52a75dd // indirect
	gorm.io/driver/postgres v1.3.1
	gorm.io/gorm v1.23.3
)
