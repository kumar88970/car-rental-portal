package main

import (
	// "database/sql"
	"context"
	"encoding/json"
	"fmt"
	"reflect"

	"strconv"
	"time"

	"github.com/go-redis/redis"
	

	"github.com/google/uuid"

	"github.com/segmentio/kafka-go"

	// "log"

	"net/http"

	"github.com/julienschmidt/httprouter"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	// "html/template"
	// "strings"
	// "github.com/google/uuid"
	// "github.com/gorilla/mux"
	// _ "github.com/lib/pq"
	// "gorm.io/driver/postgres"
)

var db *gorm.DB
var err error

var client=red()




type Response struct {
	Code    string `json:"id,omitempty"`
	Present bool   `json:"present"`
}

type booking_table struct {
	gorm.Model
	Code      string `json:"code"`
	Reason    string `json:"reason"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Veh       string `json:"veh"`
	Cat       string `json:"cat"`
	Pay       string `json:"pay"`
	Day       string `json:"day"`
	Sdate     string `json:"sdate"`
	Edate     string `json:"edate"`
	Seats     string `json:"seats"`
	Hour      string `json:"hour"`
	Min       string `json:"min"`
	Aop       string `json:"aop"`
	Amt       int `json:"amt"`
	Time       string  `json:"time"`
	Id        int  `json:"id"`
	Eg       string  `json:"eg"`
	

	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

type perinfo struct {
	Code  string
	Name  string
	Email string
	Phone string
}

type vehinfo struct {
	Code  string
	Name  string
	Veh   string
	Cat   string
	Seats string
}

type datinfo struct {
	Code      string `json:"code"`
	Name      string `json:"name"`
	Sdate     string `json:"sdate"`
	Edate     string `json:"edate"`
	Hour      string `json:"hour"`
	Min       string `json:"min"`
	CreatedAt time.Time
}

type payinfo struct {
	Code string `json:"code"`
	Name string `json:"name"`
	Pay  string `json:"pay"`
	Day  string `json:"day"`
	Amt  int
}

type statusinfo struct {
	Code  string `json:"code"`
	Name  string `json:"name"`
	Phone string `json:"phone"`
	Veh   string `json:"veh"`
	Cat   string `json:"cat"`
	Ron   string `json:"ron"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

type redget struct{
	Total []booking_table

}

func init() {

	dsn := "host=localhost user=postgres password=121 dbname=test port=5432 sslmode=disable TimeZone=Asia/Kolkata"
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	fmt.Println("Now we are connected to POSTGRESQL DATABASE.")

}


	

func red()(*redis.Client){
	
    
	client := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
		Password: "",
		DB: 0,
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)

	

	
	return client

}








const (
	topic          = "quickstart-events"
	broker1Address = "localhost:9092"
	
)


const Host = "localhost"
const Port = 5432
const User = "postgres"
const Password = "121"
const Dbname = "test"



func produce(ctx context.Context,rec booking_table) {
	
	w:=kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{broker1Address},
		Topic:   topic,
	})
	json, err := json.Marshal(rec)
	if err != nil {
		fmt.Println("Producer json Marshall :", err)
	}

			err = w.WriteMessages(ctx, kafka.Message{
			Key: []byte(rec.Code),
			
			Value: []byte(json),
		})
		if err != nil {
			panic(err)
		}

		
		fmt.Println("writes")
		
		
	}


	type handler struct {
		DB *gorm.DB
	}
	
	func New(db *gorm.DB) handler {
		return handler{db}
	}
	
	func GenerateUUID() uuid.UUID {
		id := uuid.New()
	
		return id
	}




var amt int

func  HelloWorldPage(w http.ResponseWriter,r *http.Request, _ httprouter.Params) {
	fmt.Println("Got insert post request")

	record := booking_table{}    	
 

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	err := json.NewDecoder(r.Body).Decode(&record)
	

	json.NewEncoder(w).Encode(record)
	fmt.Println(record.Code)
	if err != nil {
		panic(err)
	}
    

		//sports booking_table=1 day(10000)

	

		sdat:=(record.Sdate)+"-"
		edat:=(record.Edate)+"-"
		fmt.Println(sdat)
		fmt.Println(edat)
		fmt.Println(record.Sdate)
		fmt.Println(record.Edate)
		d1:=sdat
		d2:=edat
		e1:=d1[8:10]
		e2:=d2[8:10]
		fmt.Println(e1)
		fmt.Println(e2)
		sd, _ := strconv.Atoi(e1)
		ed, _ := strconv.Atoi(e2)
		day:=(ed-sd)+1


	if record.Cat == "Sports booking_table type(10000)" {
		// day, _ := strconv.Atoi(record.Day)

		amt = 10000 * (day)
		fmt.Print(amt)

	}else if(record.Cat == "Micro type(3500)"){

	    
		

		amt = 3500 * (day)
		fmt.Print(amt)

	}else if(record.Cat == "HatchBack(5000)"){

	    
		

		amt = 5000 * (day)
		fmt.Print(amt)

	}else if(record.Cat == "Van type(7000)"){

	    
		

		amt = 7000 * (day)
		fmt.Print(amt)

	}else if(record.Cat == "Mini Van type(6000)"){

	    
		// day, _ := strconv.Atoi(record.Day)

		amt = 6000 * (day)
		fmt.Print(amt)

	}else if(record.Cat == "RoadSter(9000)"){

	    
		// day, _ := strconv.Atoi(record.Day)

		amt = 9000 * (day)
		fmt.Print(amt)

	}
	day1:= strconv.Itoa(day)
	record.Day=day1


	time:=record.Hour+":"+record.Min+" "+record.Aop

	fmt.Print(record.Amt)


   



	
	// result := db.Select("code", "name", "email", "phone", "veh", "cat", "pay", "day1", "sdate", "edate", "seats", "hour", "min", "aop").Create(&record)
    user0:=booking_table{Code: record.Code, Name: record.Name, Email: record.Email, Phone: record.Phone, Veh: record.Veh, Cat: record.Cat,Pay: record.Pay, Day: day1,Sdate: record.Sdate, Edate: record.Edate,Seats:record.Seats, Amt:amt,Time:time}
    fmt.Println("add the no ",user0)
	result := db.Create(&user0)

	if result.Error != nil {
		panic(result.Error)

	}
	json.NewEncoder(w).Encode(record)

	ctx := context.Background()
	
	produce(ctx,record)
    
	user1 := perinfo{Code: record.Code, Name: record.Name, Email: record.Email, Phone: record.Phone}
	res1 := db.Create(&user1)
	fmt.Print(user1.Name)
	fmt.Print(res1.Error)
	fmt.Print(res1.RowsAffected, "\n")

	user2 := vehinfo{Code: record.Code, Name: record.Name, Veh: record.Veh, Cat: record.Cat, Seats: record.Seats}
	res2 := db.Create(&user2)
	fmt.Print(user2.Name)
	fmt.Print(res2.Error)
	fmt.Print(res2.RowsAffected)

	user3 := datinfo{Code: record.Code, Name: record.Name, Sdate: record.Sdate, Edate: record.Edate,Hour:record.Hour,Min:record.Min}
	res3 := db.Create(&user3)
	fmt.Print(user3.Name)
	fmt.Print(res3.Error)
	fmt.Print(res3.RowsAffected)



	user4 := payinfo{Code: record.Code, Name: record.Name, Pay: record.Pay, Day: record.Day, Amt: amt}
	res4 := db.Create(&user4)
	fmt.Print(user4.Name)
	fmt.Print(res4.Error)
	fmt.Print(res4.RowsAffected)

	user5 := statusinfo{Code: record.Code, Name: record.Name, Phone: record.Phone, Ron: "Not Returned",Veh:record.Veh,Cat:record.Cat}
	res5 := db.Create(&user5)
	fmt.Print(user5.Name)
	fmt.Print(res5.Error)
	fmt.Print(res5.RowsAffected)



	json1,err:= json.Marshal(booking_table{Code: record.Code, Name:record.Name, Email: record.Email, Phone: record.Phone, Veh: record.Veh, Cat: record.Cat,Pay:record.Pay,Day:record.Day,Sdate: record.Sdate, Edate: record.Edate,Seats:record.Seats,Time:time})
	
    if err != nil {
        fmt.Println(err)
    }
	// fmt.Print(json1)


	err = client.Set("id1234", json1, 0).Err()
     if err != nil {
         fmt.Println(err)
     }



	



	
}

func update(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got update post request")

	record := booking_table{}
	
	

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	json.NewDecoder(r.Body).Decode(&record)

	record.Id ,_= strconv.Atoi(record.Eg)

	fmt.Println("Eg value",record.Eg)

	


	fmt.Printf("exam%+v",record.Id)
	

	result1 := db.Model(booking_table{}).Where("id=?", record.Id).Updates(booking_table{Name: record.Name, Email: record.Email, Phone: record.Phone, Veh: record.Veh, Cat: record.Cat, Pay: record.Pay, Day: record.Day, Sdate: record.Sdate, Edate: record.Edate, Seats: record.Seats, Hour: record.Hour, Min: record.Min, Aop: record.Aop})
	
	if result1.Error != nil {
	    panic(result1.Error)
	}


	result2 := db.Model(statusinfo{}).Where("id=?", record.Id).Updates(statusinfo{Name: record.Name, Veh:record.Veh,Cat:record.Cat,Phone:record.Phone})
	
	if result2.Error != nil {
	    panic(result2.Error)
	}

	res := Response{}
	fmt.Print(result1.RowsAffected)

	fmt.Println(record.Id)
	fmt.Println(record.Id)

	fmt.Println(record.Name)


	if result1.RowsAffected == 0 {
	    res.Code = "0"
		fmt.Print("not found record")
	    res.Present=false
	    json.NewEncoder(w).Encode(res)
	    return
	} else {
	    res.Code=record.Code
	    res.Present=true

	    json.NewEncoder(w).Encode(res)
	    return

	}

}







func delete(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got delete request")
	record := booking_table{}


	w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    

	record.Id ,_= strconv.Atoi(record.Eg)
	fmt.Println(record.Eg)

	fmt.Println(record.Id)
	fmt.Println(record.Reason)
	
	w.Header().Set("Content-Type", "application/json")
	json.NewDecoder(r.Body).Decode(&record)
	json.NewEncoder(w).Encode(record)
	


	resu:=db.Model(booking_table{}).Where("code=?", record.Code).Updates(booking_table{Reason: record.Reason})
	if resu.Error != nil {
	    panic(resu.Error)
	}
	json.NewEncoder(w).Encode(record)

	





	result := db.Where("code=?", record.Code).Delete(&booking_table{})
	if result.Error != nil {
		panic(result.Error)
	}


	

	fmt.Print("Rows affected:", result.RowsAffected)



	// result1 := db.Where("code=?", record.Code).Delete(&datinfo{})
	// if result1.Error != nil {
	// 	panic(result1.Error)
	// }


	// result2 := db.Where("code=?", record.Code).Delete(&payinfo{})
	// if result2.Error != nil {
	// 	panic(result2.Error)
	// }
	

	result3 := db.Where("code=?", record.Code).Delete(&perinfo{})
	if result3.Error != nil {
		panic(result3.Error)
	}


	result4 := db.Where("code=?", record.Code).Delete(&statusinfo{})
	if result4.Error != nil {
		panic(result4.Error)
	}


	// result5 := db.Where("code=?", record.Code).Delete(&vehinfo{})
	// if result5.Error != nil {
	// 	panic(result5.Error)
	// }

	

}

func readall(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got read request")
	var record []booking_table
	// record:=booking_table{}
	
	
	w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    
	Success := true
	Message := "hello all"

	fmt.Print(Message)
	fmt.Println(Success)

	db.Find(&record)
	
	json.NewEncoder(w).Encode(&record)



}


func statusin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got status read request")
	var record []statusinfo
	
	w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	

	

	db.Find(&record)
	json.NewEncoder(w).Encode(&record)

}

func perin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got per read request")
	var record []perinfo
	
	w.Header().Set("Content-Type", "application/json")
     w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	

	

	db.Find(&record)
	json.NewEncoder(w).Encode(&record)

}


func vehin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got vehicle read request")
	var record []vehinfo

	w.Header().Set("Content-Type", "application/json")
     w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	

	

	db.Find(&record)
	json.NewEncoder(w).Encode(&record)

}

func datin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got date read request")
	var record []datinfo

	w.Header().Set("Content-Type", "application/json")
     w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	

	

	db.Find(&record)
	json.NewEncoder(w).Encode(&record)

}


func payin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got payment read request")
	var record []payinfo
	
	w.Header().Set("Content-Type", "application/json")
     w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

	

	

	db.Find(&record)
	fmt.Print(record)
	fmt.Print(&record)

	json.NewEncoder(w).Encode(&record)

}

func status(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Println("Got status post request")

	record := statusinfo{}
	w.Header().Set("Content-Type", "application/json")
	err := json.NewDecoder(r.Body).Decode(&record)
	json.NewEncoder(w).Encode(record)
	fmt.Println(record.Code)
	if err != nil {
		panic(err)
	}

	// result := db.Select("code", "ron").Create(&record)
	result := db.Model(statusinfo{}).Where("code=?", record.Code).Updates(statusinfo{Ron: "Returned"})

	if result.Error != nil {
		panic(result.Error)

	}

}







func redisget(w http.ResponseWriter,r *http.Request, _ httprouter.Params) {
	
	fmt.Println("Got get redis get customer request")
	

	w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    



	
}




func get(w http.ResponseWriter,r *http.Request, _ httprouter.Params) {
	
	fmt.Println("Got get customer request")
	record:=booking_table{}

	w.Header().Set("Content-Type", "application/json")
    w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Credentials", "true")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    



	
	
	
	err := json.NewDecoder(r.Body).Decode(&record)
	if err!=nil{
		panic(err)
	}
	

	json.NewEncoder(w).Encode(record)
	fmt.Println(record.Code)

    json2:=db.Where("code=?",record.Code).First(&record)

	
	json1,err:=json.Marshal(booking_table{Code: record.Code, Name:record.Name, Email: record.Email, Phone: record.Phone, Veh: record.Veh, Cat: record.Cat,Pay:record.Pay,Day:record.Day,Sdate: record.Sdate, Edate: record.Edate,Seats:record.Seats, Hour: record.Hour, Min: record.Min})
	// json1,err:= json.Marshal(booking_table{Code: record.Code, Name:record.Name, Email: record.Email, Phone: record.Phone, Veh: record.Veh, Cat: record.Cat,Pay:record.Pay,Day:record.Day,Sdate: record.Sdate, Edate: record.Edate,Seats:record.Seats, Hour: record.Hour, Min: record.Min})
	
    if err != nil {
        fmt.Println(err)
    }
	fmt.Println(json2)
	fmt.Print(json1)


	err = client.Set("id12", json1, 0).Err()
     if err != nil {
         fmt.Println(err)
     }
}




	
func sumsg2(w http.ResponseWriter,r *http.Request, _ httprouter.Params){
	record:=booking_table{}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	
	
	fmt.Println("Got sumsg2 customer request")

	
    val, err := client.Get("id12").Result()
    if err != nil {
        fmt.Println(err)
    }
	json.Unmarshal([]byte(val),&record)
	
    fmt.Println(reflect.TypeOf(record))
	



    

	
	json.NewEncoder(w).Encode(record)

}




func sumsg3(w http.ResponseWriter,r *http.Request, _ httprouter.Params){
	var record []booking_table
	R:=redget{}
	A:=redget{}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	fmt.Println("Got  read sumsg3 customer request")
	// json2:=db.Find(&record)
	db.Find(&record)
	R.Total=record
	json1,err:=json.Marshal(R)
	// json1,err:= json.Marshal(booking_table{Code: record.Code, Name:record.Name, Email: record.Email, Phone: record.Phone, Veh: record.Veh, Cat: record.Cat,Pay:record.Pay,Day:record.Day,Sdate: record.Sdate, Edate: record.Edate,Seats:record.Seats, Hour: record.Hour, Min: record.Min})
	
    if err != nil {
        fmt.Println(err)
    }
	// fmt.Println(json2)
	// fmt.Print(json1)
	err = client.Set("id123", json1, 0).Err()
     if err != nil {
         fmt.Println(err)
     }

    val, err := client.Get("id123").Result()
    if err != nil {
        fmt.Println(err)
    }
	json3:=json.Unmarshal([]byte(val),&A)
	
    fmt.Println(reflect.TypeOf(record))

	fmt.Println(json3)
	json.NewEncoder(w).Encode(A.Total)

}


func sumsg(w http.ResponseWriter,r *http.Request, _ httprouter.Params) {
	
	c:=booking_table{}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers:", "Origin, Content-Type, X-Auth-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	
	
	fmt.Println("Got sumsg customer request")

	
    val, err := client.Get("id1234").Result()
    if err != nil {
        fmt.Println(err)
    }
	json.Unmarshal([]byte(val),&c)
	
    fmt.Println(reflect.TypeOf(c))



 

	
	json.NewEncoder(w).Encode(c)

}


func main() {
	router := httprouter.New()
	router.POST("/", HelloWorldPage)
	router.POST("/update", update)
	router.POST("/delete", delete)
	router.GET("/readall", readall)
	router.POST("/get", get)
	router.POST("/status", status)
	router.GET("/statusinfo", statusin)
	router.GET("/sumsg",sumsg)
	router.GET("/sumsg2",sumsg2)
	router.GET("/sumsg3",sumsg3)
	router.GET("/redisget",redisget)

	router.GET("/perinfo", perin)
	router.GET("/vehinfo", vehin)
	router.GET("/datinfo", datin)
	router.GET("/payinfo", payin)
	// router.POST("/login",Hello)
	http.ListenAndServe(":8080", router)

}



// {
//     "code":"123",
//     "name":"kumar",
//     "email":"a@gmail.com",
//     "phone":"123444555",
//     "veh":"booking_table",
//     "sdate":"09-10-2001",
//     "edate":"04-04-2001",
//     "seats":"4",
//     "hour":"1",
//     "min":"45",
//     "aop":"am"

//     }
