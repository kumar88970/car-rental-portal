package main

import (
	"context"
	"log"
	"strconv"

	"net/smtp"

	"encoding/json"
	"time"

	"fmt"

	"github.com/segmentio/kafka-go"
)

const (
	topic          = "quickstart-events"
	broker1Address = "localhost:9092"
	
)




type car struct {

	Code      string `json:"code"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Veh       string `json:"veh"`
	Cat       string `json:"cat"`
	Pay       string `json:"pay"`
	Day       string `json:"day"`
	Sdate    string `json:"sdate"`
	Edate     string `json:"edate"`
	Seats     string `json:"seats"`
	Hour      string `json:"hour"`
	Min       string `json:"min"`
	Aop       string `json:"aop"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}




func SendMail(to_mail string,m string) {
	// Configuration
	from := "rentalcar34@gmail.com"
	password := "carrent34@"
	to := []string{to_mail}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"
	
	

	message := []byte(m)
	
	

	
	auth := smtp.PlainAuth("", from, password, smtpHost)

	
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		log.Fatal(err)
	}
}



func consume(ctx context.Context) {
		r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{broker1Address},
		Topic:   topic,
		GroupID: "my-group",
	})
	for {
		// the `ReadMessage` method blocks until we receive the next event
		msg, err := r.ReadMessage(ctx)
		if err != nil {
			panic("could not read message " + err.Error())
		}
		// after receiving the message, log its value
        
        var record car
		e := json.Unmarshal(msg.Value, &record)
		if e != nil {
			fmt.Println("Consumer json Marshall :", err)
		}
		var amt int
		var amount string
                        
		sdat:=(record.Sdate)+"-+"
		edat:=(record.Edate)+"-+"
		fmt.Println(sdat)
		fmt.Println(edat)
		fmt.Println(record.Sdate)
		fmt.Println(record.Edate)
		d1:=sdat
		d2:=edat
		e1:=d1[8:10]
		e2:=d2[8:10]
		fmt.Println(e1)
		fmt.Println(e2)
		sd, _ := strconv.Atoi(e1)
		ed, _ := strconv.Atoi(e2)
		day:=(ed-sd)+1




		// var days int
		if record.Cat == "Sports Car type(10000)" {
		
			
			// day, _ := strconv.Atoi(record.Day)
	
			amt = 10000 * (day)
			amount=strconv.Itoa(amt)

			

			fmt.Print(amount)
	
		}else if(record.Cat == "Micro type(3500)"){
	
			
			
	
			amt = 3500 * (day)
			amount=strconv.Itoa(amt)

			

			fmt.Print(amount)
	
		}else if(record.Cat == "HatchBack(5000)"){
	
			
			
	
			amt = 5000 * (day)
		    amount=strconv.Itoa(amt)

			

			fmt.Print(amount)
	
		}else if(record.Cat == "Van type(7000)"){
	
			
			
	
			amt = 7000 * (day)
			amount=strconv.Itoa(amt)

			

			fmt.Print(amount)
	
		}else if(record.Cat == "Mini Van type(6000)"){
	
			
			
	
			amt = 6000 * (day)
			amount=strconv.Itoa(amt)

			

			fmt.Print(amount)
	
		}else if(record.Cat == "RoadSter(9000)"){
	
			
			
	
			amt = 9000 * (day)
			amount=strconv.Itoa(amt)

			

			fmt.Print(amount)
	
		}

		day1:= strconv.Itoa(day)

		message := "To: " + record.Email + "\r\n" +
			"Subject: Booking Confirmation on Car rental on " + "(" + time.Now().String() + ")\r\n" +
			"\r\n" +
			"Hello customer,\n   Thank you for using Car rental service for your car need. Your booking details are indicated below\n\n\nCustomer Code: " + record.Code + "\n\nCustomer Name: " + record.Name + "\n\nEmail : " + record.Email +  "\n\nPhone number : " + record.Phone+ "\n\nVehicle Brand : " + record.Veh + "\n\nVehical Category : " + record.Cat  +"\n\nNo of Days:"+day1  + "\n\n your Payment mode : " + record.Pay + "\n\nStart date of Rent:" + record.Sdate  + "\n\nEnd date of rent :" + record.Edate + "\n\n No of seats :" + record.Seats + "\n\n Amount :" + amount
		SendMail(record.Email, message)

		

		
	}
}








func main() {
	
	ctx := context.Background()
	
   
	consume(ctx)
}