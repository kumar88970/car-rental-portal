import React from 'react'
import JsonData from './data.json'
function JsonDataDisplay(){
	const DisplayData=JsonData.map(
		(info)=>{
			return(
				<tr>
					<td>{info.code}</td>
					<td>{info.name}</td>
					<td>{info.email}</td>
					<td>{info.phone}</td>
					<td>{info.veh}</td>
                    <td>{info.cat}</td>
					<td>{info.pay}</td>
					<td>{info.day}</td>
					<td>{info.sdate}</td>
					<td>{info.edate}</td>
					<td>{info.seats}</td>
					<td>{info.hour}</td>
					<td>{info.min}</td>
					<td>{info.aop}</td>
					
					<td>{info.created_at}</td>
					<td>{info.updated_at}</td>
					<td>{info.deleted_at}</td>
                    


				</tr>
			)
		}
	)

	return(
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th>CUSTOMER CODE</th>
					<th>NAME</th>
					<th>EMAIL</th>
					<th>PHONE</th>
					<th>BOOKED VEHICLE Brand</th>
                    <th>Car Category</th>
                    <th>Payment mode</th>
                    <th>No of Days</th>
					<th>START DATE</th>
					<th>END DATE</th>
					<th>SEATS</th>
					<th>HOUR</th>
					<th>MIN</th>
					<th>AM/PM</th>
                    <th>Created Date</th>
                    <th>Updated Date</th>
					
					</tr>
				</thead>
				<tbody>
				
					
					{DisplayData}
					
				</tbody>
			</table>
			
		</div>
	)
}

export default JsonDataDisplay;
