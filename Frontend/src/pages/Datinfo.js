import React, { useState } from 'react';
import Hom from './Hom';
import Admin from './Status';
import Update from './Update';
import Delete from './Delete';
import { BrowserRouter as Router, Route, Link, NavLink, Switch } from "react-router-dom";

export default function Datinfo() {

    const [data, setData] = useState(
       

        []


    )

    function Save(e) {
        e.preventDefault()


        fetch("http://localhost:8080/readall").then(data => {


            return data.json()

        })
            .then(data => {
                console.log(data);
                setData(data)
            })
    }






    return (

        <div>
            <div>
            <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>CUSTOMER CODE</th>
                            <th>NAME</th>
                            
                            
                            <th>START DATE</th>
                            <th>END DATE</th>
                            
                            <th>HOUR</th>
                            <th>MIN</th>
                            <th>AM/PM</th>
                            
                           
                            {/* <th>Created Date</th> */}
                          

                        </tr>
                    </thead>



                    {data.map((info) => {


                        return (

                            <tbody>

                                <tr>
                                    <td>{info.code}</td>
                                    <td>{info.name}</td>
                                    
                                    <td>{info.sdate}</td>
                                    <td>{info.edate}</td>
                                    
                                    <td>{info.hour}</td>
                                    <td>{info.min}</td>
                                    <td>{info.aop}</td>
                                   
                                    
                                    <td>{info.created_at}</td>
                                    
                                </tr>

                            </tbody>
                        )
                    })}

                </table>

              </div>

            <div id="booking" className="section">
                <div className="section-center">
                    <div className="container">
                        <div className="row">
                            <div className="booking-form">
                                <div className="form-header">
                                    <h1>click read button to get all customer details</h1>
                                </div>
                                <form onSubmit={Save} >
                                    {/* <input type="submit" name="submit" value="Book Now" > */}
                                    <div className="form-btn">
                                        {/* <button class="submit-btn">Book Now</button>  */}
                                        <button id="mybutton" type="submit" className="submit-btn" style={{ backgroundColor: 'yellow' }} name="submit" value="Read">Read</button>



                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>


    );
}