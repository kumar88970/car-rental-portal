import React, { useState } from 'react';


// class Hom extends React.Component {
export default function Hom(){



  function handle(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    console.log(newdata)
    setData(newdata)
  }



  // render(){

  const [data, setData] = useState(
    {
      code: "",
      name: "",
      email: "",
      phone: "",
      veh: "",
      cat: "",
      pay: "",
      day: "",
      sdate: "",
      edate: "",
      seats: "",
      hour: "",
      min: "",
      aop: ""
    }
  )
  const Mes="hello"

 function Save(e) {


    // localStorage.setItem("code",data.code)
    // localStorage.setItem("name",data.name)
    // localStorage.setItem("email",data.email)
    // localStorage.setItem("phone",data.phone)
    // localStorage.setItem("veh",data.veh)
    // localStorage.setItem("cat",data.cat)
    // localStorage.setItem("pay",data.pay)
    // localStorage.setItem("day",data.day)
    // localStorage.setItem("sdate",data.sdate)
    // localStorage.setItem("edate",data.edate)
    // localStorage.setItem("seats",data.seats)
    


    fetch("http://localhost:8080/", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'

      },
      body: JSON.stringify(data)

    }).then(function (response) {
      console.log(response)
    })

  }


  


  

  return (
    



    <div>


      <div id="booking" className="section">
        <div className="section-center">
          <div className="container">
            <div className="row">
              <div className="booking-form">
                <div className="form-header">
                  <h1>Bookings will be made by Admin</h1>
                </div>

                <form onSubmit={Save} action="/sumsg.html">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Customer code</span>
                        <input pattern=".{10}" pattern="[0-9]" id="code" onChange={(e) => handle(e)} value={data.code} className="form-control" name="code" type="number" placeholder="Enter your own code" required/>
                        {/* <label form="name" >Name:</label>  */}
                        {/* <input type="text"   name="name" placeholder="Enter your name" style=" height: 45px;"> */}
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Name</span>
                        <input id="name" onChange={(e) => handle(e)} value={data.name} className="form-control" name="name" type="text" placeholder="Enter your name" required/>
                        {/* <input type="text"   name="email" placeholder="Enter your E mail" style=" height: 45px;"> */}
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <span className="form-label">Email</span>
                    <input pattern=".+@gmail\.com" size="30" id="email" onChange={(e) => handle(e)} value={data.email} className="form-control" name="email" type="email" placeholder="Enter your Email" required />
                    {/* <input type="tel"   name="phone" placeholder="Enter your Phone number" style=" height: 45px;"> */}
                  </div>
                  <div className="form-group">
                    <span className="form-label">Phone</span>
                    <input pattern=".{10}" pattern="[6789][0-9]{9}" id="phone" onChange={(e) => handle(e)} value={data.phone} className="form-control" name="phone" type="tel" placeholder="Enter your phone number" required />
                    {/* <input type="text"   name="ploc" placeholder="Enter your Pickup location" style=" height: 45px;"> */}
                  </div>
                  <div className="col-sm-14">
                    <div className="form-group">
                      <span className="form-label">Enter Car Brand</span>
                      <select id="veh" onChange={(e) => handle(e)} value={data.veh} className="form-control" name="veh"  required>
                        <option>Optional</option>
                        <option>Toyota </option>
                        <option>Ford Motor</option>
                        <option>Honda</option>
                        <option>Maruti Suzuki</option>
                        <option>Tata</option>
                        <option>Tesla</option>
                      </select>
                      <span className="select-arrow" />
                    </div>
                  </div>
                  <div className="col-sm-14">
                    <div className="form-group">
                      <span className="form-label">Category of car</span>
                      <select id="cat" onChange={(e) => handle(e)} value={data.cat} className="form-control" name="cat"  required>
                        <option>Optional</option>
                        <option>Sports Car type(10000)</option>
                        <option>Micro type(3500)</option>
                        <option>HatchBack(5000)</option>
                        <option>Van type(7000)</option>
                        <option>Mini Van type(6000)</option>
                        <option>RoadSter(9000)</option>
                        
                      </select>
                      <span className="select-arrow" />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Enter Payment Mode</span>
                        <select id="pay" onChange={(e) => handle(e)} value={data.pay} className="form-control" name="pay"  required>
                          <option>NA</option>
                          <option>online mode</option>
                          <option>Hand Cash</option>

                        </select>
                        <span className="select-arrow" />
                      </div>

                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <div className="form-group">
                          <span className="form-label">No of days(Optional)</span>
                          <select id="day" onChange={(e) => handle(e)} value={data.day} className="form-control" name="day"  required>
                            <option>NA</option>
                            
                            <option>2</option>
                            <option>4</option>
                            <option>6</option>
                            <option>8</option>
                            <option>10</option>
                            <option>12</option>
                            <option>14</option>
                            <option>20</option>



                          </select>
                          <span className="select-arrow" />
                        </div>
                      </div>

                    </div>
                  </div>


                  <div className="row">
                    <div className="col-sm-6">
          
                      <div className="form-group">
                        <span className="form-label">Need rent from</span>
                        <input  min="2022-03-21"    id="sdate" onChange={(e) => handle(e)} value={data.sdate} className="form-control" name="sdate" type="date" required/>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Need rent up to</span>
                        <input min="2022-03-22"  max="2022-04-22"   id="edate" onChange={(e) => handle(e)} value={data.edate} className="form-control" name="edate" type="date" required/>
                      </div>
                    </div>
                    <div className="row">

                    <div className="col-sm-9">
                    <div className="col-sm-3">
                      <div className="form-group">
                        <span className="form-label">Seats</span>

                        <select id="seats" onChange={(e) => handle(e)} value={data.seats} className="form-control" name="seats"  required>

                          <option>4</option>

                          <option>6</option>

                          <option>8</option>

                          <option>10</option>
                        </select>
                        <span className="select-arrow" />
                      </div>
                    </div>
                    


                        <div className="col-sm-3">
                          <div className="form-group">
                            <span className="form-label">Hour</span>
                            <select id="hour" onChange={(e) => handle(e)} value={data.hour} className="form-control" name="hour"  required>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              <option>8</option>
                              <option>9</option>
                              <option>10</option>
                              <option>11</option>
                              <option>12</option>
                            </select>
                            <span className="select-arrow" />
                          </div>
                        </div>
                        <div className="col-sm-3">
                          <div className="form-group">
                            <span className="form-label">Min</span>
                            <select id="min" onChange={(e) => handle(e)} value={data.min} className="form-control" name="min" required>
                              <option>05</option>
                              <option>10</option>
                              <option>15</option>
                              <option>20</option>
                              <option>25</option>
                              <option>30</option>
                              <option>35</option>
                              <option>40</option>
                              <option>45</option>
                              <option>50</option>
                              <option>55</option>
                            </select>
                            <span className="select-arrow" />
                          </div>
                        </div>
                        <div className="col-sm-3">
                          <div className="form-group">
                            <span className="form-label">AM/PM</span>
                            <select id="aop" onChange={(e) => handle(e)} value={data.aop} className="form-control" name="aop"  required>
                              <option>NA</option>
                              
                              <option>AM</option>

                              <option>PM</option>
                            </select>
                            <span className="select-arrow" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <input type="submit" name="submit" value="Book Now" > */}
                  <div className="form-btn">
                    {/* <button class="submit-btn">Book Now</button>  */}
                    <button className="submit-btn" type="submit" style={{ backgroundColor: 'yellow' }} name="submit" value="Book Now">SUBMIT</button>
                  </div>
                </form>

                <div>
                  hello
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



    </div>
  );
}


// export default Hom;



