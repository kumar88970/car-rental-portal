import React, { useState } from 'react';
export default function Delete() {
    function handle(e) {
        const newdata = { ...data }
        newdata[e.target.id] = e.target.value
        console.log(newdata)
        setData(newdata)
      }
    
    
    
      // render(){
    
      const [data, setData] = useState(
        {
          
         
          code: "",
          reason:"",
          name: "",
          email: "",
          phone: "",
          veh: "",
          sdate: "",
          edate: "",
          seats: "",
          hour: "",
          min: "",
          aop: ""
        }
      )
    
      function save(e) {
    
    
    
    
        fetch("http://localhost:8080/delete", {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
    
          },
          body: JSON.stringify(data)
    
        }).then(function (response) {
          console.log(response)
        })
    
      }
      return (
        <div>
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    
          <div id="booking" className="section">
            <div className="section-center">
              <div className="container">
                <div className="row">
                  <div className="booking-form">
                    <div className="form-header">
                      <h1>Delete your details with your customer code</h1>
                    </div>
    
                    <form onSubmit={save} action="/demsg.html">
                      <div className="row">
                        <div className="col-sm-12">
                          <div className="form-group">
                            <span className="form-label">Customer code</span>
                            <input id="code" onChange={(e) => handle(e)} value={data.code} className="form-control" name="code" type="text" placeholder="Enter Customer Code" required/>
                            {/* <input id="eg" onChange={(e) => handle(e)} value={data.eg} className="form-control" name="eg" type="text" placeholder="Enter Customer ID" required/> */}
                            {/* <label form="name" >Name:</label>  */}
                            {/* <input type="text"   name="name" placeholder="Enter your name" style=" height: 45px;"> */}
                          </div>
                        </div>
                      </div>


                      <div className="col-sm-14">
                          <div className="form-group">
                            <span className="form-label">Reason</span>
                            <input id="reason" onChange={(e) => handle(e)} value={data.reason} className="form-control" name="reason" type="text" placeholder="Enter Reason for why you are deleting" required/>
                            {/* <label form="name" >Name:</label>  */}
                            {/* <input type="text"   name="name" placeholder="Enter your name" style=" height: 45px;"> */}
                          </div>
                        </div>
                
                   
                  
                      {/* <input type="submit" name="submit" value="Book Now" > */}
                      <div className="form-btn">
                        {/* <button class="submit-btn">Book Now</button>  */}
                       <button className="submit-btn" type="submit" style={{ backgroundColor: 'yellow' }} name="submit" value="delete">Delete</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
    
    
        </div>
      );
}