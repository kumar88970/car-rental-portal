import React, { useState } from 'react';

import Hom from './Hom';
import Read from './Read';
import Header from './Header';
import Delete from './Delete';
import Status from './Status';
import First from './First';
import Get from './Get';
import { BrowserRouter as Router, Route, Link, NavLink, Switch } from "react-router-dom";
// class Hom extends React.Component {
export default function Update() {


  function handle(e) {
    const newdata = { ...data }
    newdata[e.target.id] = e.target.value
    console.log(newdata)
    setData(newdata)
  }



  // render(){

  

  const [data, setData] = useState(
    {
      
      eg:"",
      name: "",
      email: "",
      phone: "",
      veh: "",
      cat:"",
      pay:"",
      day:"",
      sdate: "",
      edate: "",
      seats: "",
      hour: "",
      min: "",
      aop: ""
    }
  )

  function save(e) {




    fetch("http://localhost:8080/update", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'

      },
      body: JSON.stringify(data)

    }).then(function (response) {
      console.log(response)
    })

  }
  return (
    <div>
















    









      <div id="booking" className="section">
        <div className="section-center">
          <div className="container">
            <div className="row">
              <div className="booking-form">
                <div className="form-header">
                  <h1>Update your details here</h1>
                </div>

                <form onSubmit={save} action="/upmsg.html">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Customer ID</span>
                        <input id="eg" onChange={(e) => handle(e)} value={data.eg} className="form-control" name="eg" type="text" placeholder="Enter your ID" required/>
                        {/* <label form="name" >Name:</label>  */}
                        {/* <input type="text"   name="name" placeholder="Enter your name" style=" height: 45px;"> */}
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Name</span>
                        <input id="name" onChange={(e) => handle(e)} value={data.name} className="form-control" name="name" type="text" placeholder="Enter your name" />
                        {/* <input type="text"   name="email" placeholder="Enter your E mail" style=" height: 45px;"> */}
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <span className="form-label">Email</span>
                    <input id="email" onChange={(e) => handle(e)} value={data.email} className="form-control" name="email" type="email" placeholder="Enter your Email" />
                    {/* <input type="tel"   name="phone" placeholder="Enter your Phone number" style=" height: 45px;"> */}
                  </div>
                  <div className="form-group">
                    <span className="form-label">Phone</span>
                    <input id="phone" onChange={(e) => handle(e)} value={data.phone} className="form-control" name="phone" type="tel" placeholder="Enter your phone number" />
                    {/* <input type="text"   name="ploc" placeholder="Enter your Pickup location" style=" height: 45px;"> */}
                  </div>
                  <div className="col-sm-14">
                    <div className="form-group">
                      <span className="form-label">Type of vehicle</span>
                      <select id="veh" onChange={(e) => handle(e)} value={data.veh} className="form-control" name="veh">
                        <option>Car</option>
                        <option>Jeep</option>
                        <option>Bus</option>
                        <option>Camper van</option>
                        <option>Mini van</option>
                        <option>Truck</option>
                      </select>
                      <span className="select-arrow" />
                    </div>
                  </div>
                  <div className="col-sm-14">
                    <div className="form-group">
                      <span className="form-label">Category of car</span>
                      <select id="cat" onChange={(e) => handle(e)} value={data.cat} className="form-control" name="cat">
                        <option>Optional</option>
                        <option>Car</option>
                        <option>Jeep</option>
                        <option>Bus</option>
                        <option>Camper van</option>
                        <option>Mini van</option>
                        <option>Truck</option>
                      </select>
                      <span className="select-arrow" />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label">Enter Payment Mode</span>
                      <select id="pay" onChange={(e) => handle(e)} value={data.pay} className="form-control" name="pay">
                        <option>NA</option>
                        <option>online mode</option>
                        <option>Hand Cash</option>
                       
                      </select>
                      <span className="select-arrow" />
                    </div>
                     
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                      <div className="form-group">
                      <span className="form-label">No of days</span>
                      <select id="day" onChange={(e) => handle(e)} value={data.day} className="form-control" name="day">
                        
                        <option>2</option>
                        <option>4</option>
                        <option>6</option>
                        <option>8</option>
                        <option>10</option>
                        <option>12</option>
                        <option>14</option>
                        <option>20</option>
                      
                      
                       
                      </select>
                      <span className="select-arrow" />
                    </div>
                    </div>
                        
                    </div>
                    </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Need rent from</span>
                        <input id="sdate" onChange={(e) => handle(e)} value={data.sdate} className="form-control" name="sdate" type="date" />
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <span className="form-label">Need rent up to</span>
                        <input id="edate" onChange={(e) => handle(e)} value={data.edate} className="form-control" name="edate" type="date" />
                      </div>
                    </div>
                    <div className="col-sm-12">
                      <div className="form-group">
                        <span className="form-label">Seats</span>

                        <select id="seats" onChange={(e) => handle(e)} value={data.seats} className="form-control" name="seats">

                          <option>4</option>
                          <option>5</option>
                          <option>6</option>
                          <option>7</option>
                          <option>8</option>
                          <option>9</option>
                          <option>10</option>
                        </select>
                        <span className="select-arrow" />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-sm-9">
                        <div className="col-sm-4">
                          <div className="form-group">
                            <span className="form-label">Hour</span>
                            <select id="hour" onChange={(e) => handle(e)} value={data.hour} className="form-control" name="hour">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              <option>8</option>
                              <option>9</option>
                              <option>10</option>
                              <option>11</option>
                              <option>12</option>
                            </select>
                            <span className="select-arrow" />
                          </div>
                        </div>
                        <div className="col-sm-4">
                          <div className="form-group">
                            <span className="form-label">Min</span>
                            <select id="min" onChange={(e) => handle(e)} value={data.min} className="form-control" name="min" required>
                              <option>05</option>
                              <option>10</option>
                              <option>15</option>
                              <option>20</option>
                              <option>25</option>
                              <option>30</option>
                              <option>35</option>
                              <option>40</option>
                              <option>45</option>
                              <option>50</option>
                              <option>55</option>
                            </select>
                            <span className="select-arrow" />
                          </div>
                        </div>
                        <div className="col-sm-4">
                          <div className="form-group">
                            <span className="form-label">AM/PM</span>
                            <select id="aop" onChange={(e) => handle(e)} value={data.aop} className="form-control" name="aop">
                              <option>AM</option>
                              <option>PM</option>
                            </select>
                            <span className="select-arrow" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <input type="submit" name="submit" value="Book Now" > */}
                  <div className="form-btn">
                    {/* <button class="submit-btn">Book Now</button>  */}
                   <button className="submit-btn" type="submit" style={{ backgroundColor: 'yellow' }} name="submit" value="update">Update</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>



    </div>
  );
}